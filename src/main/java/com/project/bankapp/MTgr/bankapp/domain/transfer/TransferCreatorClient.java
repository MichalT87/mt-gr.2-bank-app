package com.project.bankapp.MTgr.bankapp.domain.transfer;

public interface TransferCreatorClient {

    void create(Transfer transfer);
}