package com.project.bankapp.MTgr.bankapp.domain.account;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.project.bankapp.MTgr.bankapp.domain.user.UserRetrievalClient;

@Slf4j
@Service
@RequiredArgsConstructor
class AccountCreator {

    private final UserRetrievalClient userRetrievalClient;

    @Transactional
    public Account createStandardAccountAndAssignToUser(long userId) {
        Account regularAccount = Account.generateStandardAccount();
        userRetrievalClient.findById(userId).addAccount(regularAccount);
        log.info("Create standard account for user with id: {}.", userId);
        return regularAccount;
    }

    @Transactional
    public Account createPremiumAccountAndAssignToUser(long userId) {
        Account premiumAccount = Account.generatePremiumAccount();
        userRetrievalClient.findById(userId).addAccount(premiumAccount);
        log.info("Create premium account for user with id: {}.", userId);
        return premiumAccount;
    }
}