package com.project.bankapp.MTgr.bankapp.domain.account;

public class AccountException extends RuntimeException {

    public AccountException(String message) {
        super(message);
    }

    public static AccountException accountNotFound(long id){
        return new AccountException(String.format
                ("Account not found [id=%s]", id));
    }
}