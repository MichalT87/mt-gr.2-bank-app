package com.project.bankapp.MTgr.bankapp.domain.account;

public enum AccountType {
    PREMIUM, STANDARD
}