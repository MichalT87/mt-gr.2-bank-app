package com.project.bankapp.MTgr.bankapp.domain.card;

public enum Status {
    ACTIVE, RESTRICTED
}