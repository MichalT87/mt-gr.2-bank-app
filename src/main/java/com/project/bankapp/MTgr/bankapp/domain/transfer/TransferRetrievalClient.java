package com.project.bankapp.MTgr.bankapp.domain.transfer;

import java.util.List;

public interface TransferRetrievalClient {

    List<TransferCreatorCommand> getPendingTransfers();
}