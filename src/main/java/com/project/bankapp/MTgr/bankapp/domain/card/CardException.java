package com.project.bankapp.MTgr.bankapp.domain.card;

public class CardException extends RuntimeException {

    public CardException(String message) {
        super(message);
    }

    public static CardException cardAlreadyRestricted(long accountId, String lastFourDigits) {
        return new CardException(String.format
                ("Card with last four digits: {} from account with id: {} do not exist or has been already restricted.", lastFourDigits, accountId));
    }
}