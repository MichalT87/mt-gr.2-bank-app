package com.project.bankapp.MTgr.bankapp.infastructure.transfer;

import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferStatusResponseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class StatusResponse implements TransferStatusResponseClient {

    private RestTemplate restTemplate;
    private String statusChangeUrl;

    @Autowired
    public StatusResponse(RestTemplate restTemplate,
                          @Value("http://${order.status.changer.url}") String statusChangeUrl) {
        this.restTemplate = restTemplate;
        this.statusChangeUrl = statusChangeUrl;
    }

    @Override
    public void sendStatusChange(long id) {
        restTemplate.exchange(String.format("%s/%d/status", statusChangeUrl, id),
                HttpMethod.POST,
                HttpEntity.EMPTY,
                Void.class);
    }
}