package com.project.bankapp.MTgr.bankapp.api.user;

import com.project.bankapp.MTgr.bankapp.domain.user.UserCreatorCommand;
import com.project.bankapp.MTgr.bankapp.domain.user.UserCreatorCommandMapper;
import com.project.bankapp.MTgr.bankapp.domain.user.UserFacade;
import com.project.bankapp.MTgr.bankapp.domain.user.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
class UserController {

    private final UserFacade userFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@Valid @RequestBody UserRequest userRequest) {
        UserCreatorCommand userCreatorCommand = UserCreatorCommandMapper.createUserCommand(userRequest);
        userFacade.createUser(userCreatorCommand);
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable long userId) {
        try {
            UserResponse userResponse = userFacade.getUserById(userId);
            return ResponseEntity.ok(userResponse);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/email/{email}")
    public ResponseEntity<UserResponse> getUserByEmail(@PathVariable String email) {
        try {
            UserResponse userResponse = userFacade.getUserByEmail(email);
            return ResponseEntity.ok(userResponse);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        return ResponseEntity.ok(userFacade.getAllUsers());
    }
}