package com.project.bankapp.MTgr.bankapp.domain.user;

public interface UserCreatorClient {

    void create(User user);
}