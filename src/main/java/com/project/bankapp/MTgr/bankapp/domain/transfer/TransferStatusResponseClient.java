package com.project.bankapp.MTgr.bankapp.domain.transfer;

public interface TransferStatusResponseClient {

    void sendStatusChange(long id);
}