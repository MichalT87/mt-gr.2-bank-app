package com.project.bankapp.MTgr.bankapp.domain.transfer;

import com.project.bankapp.MTgr.bankapp.domain.account.Account;
import com.project.bankapp.MTgr.bankapp.domain.account.AccountRetrievalClient;
import com.project.bankapp.MTgr.bankapp.domain.account.FeesCharger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class TransferExecutor {

    private final AccountRetrievalClient accountRetrievalClient;
    private final TransferCreatorClient transferCreatorClient;

    @Transactional
    void createAndExecuteTransfer(TransferCreatorCommand transferCreatorCommand) {
        Transfer transfer = Transfer.createTransferOfMoney(transferCreatorCommand);
        Account fromAccount = accountRetrievalClient.findById(transfer.getSenderAccountId());
        fromAccount.accountHasAmountOrThrow(transfer.getAmount());
        fromAccount.reduceBalance(transfer.getAmount());
        findAccountAndAddOrTransferToClientsBank(transfer);
        transferCreatorClient.create(transfer);
        log.info("Transfer created: {}.", transfer.toString());
    }

    private void findAccountAndAddOrTransferToClientsBank(Transfer transfer) {
        Optional<Account> clientAccount = accountRetrievalClient.findByAccountNumber(
                transfer.getRecipientAccountNumber());
        if (!clientAccount.isPresent()) {
            log.info("Account from other bank!");
            FeesCharger.addExternalTransferFee(transfer.getOwnerId());
        } else {
            clientAccount.get().increaseBalance(transfer.getAmount());
        }
    }
}