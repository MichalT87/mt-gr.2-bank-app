package com.project.bankapp.MTgr.bankapp.domain.user;

import com.project.bankapp.MTgr.bankapp.domain.account.Account;
import com.project.bankapp.MTgr.bankapp.shared.Auditable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter(value = AccessLevel.PRIVATE)
@Table(name = "USERS")
public class User extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_sequence")
    @SequenceGenerator(name = "user_sequence")
    private long id;
    @Setter(value = AccessLevel.PRIVATE)
    private String firstName;
    @Setter
    private String lastName;
    @Setter
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Setter
    @Column(unique = true)
    private String login;
    @NotBlank
    private String password;
    @Email
    private String email;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Account> accounts = new HashSet<>();

    public void addAccount(Account account) {
        accounts.add(account);
        account.setUser(this);
    }

    public static User createUser(UserCreatorCommand userCreatorCommand) {
        User user = new User();
        user.setFirstName(userCreatorCommand.getFirstName());
        user.setLastName(userCreatorCommand.getLastName());
        user.setGender(GenderSet(userCreatorCommand.getGender()));
        user.setLogin(userCreatorCommand.getLogin());
        user.setPassword(userCreatorCommand.getPassword());
        user.setEmail(userCreatorCommand.getEmail());
        return user;
    }

    public UserResponse generateResponse() {
        return UserCreatorCommandMapper.mapToDto(this);
    }

    public static Gender GenderSet(String gender) {
        if (gender.equals("M")) {
            return Gender.MALE;
        } else if (gender.equals("F")) {
            return Gender.FEMALE;
        } else {
            throw new IllegalArgumentException(String
                    .format("Unknown gender: [%d].", gender));
        }
    }
}