package com.project.bankapp.MTgr.bankapp.domain.transfer;

import com.project.bankapp.MTgr.bankapp.api.transfer.TransferRequest;

public class TransferCreatorCommandMapper {

    public static TransferCreatorCommand createTransferCommand(TransferRequest transferRequest) {
        return TransferCreatorCommand.builder()
                .id(transferRequest.getId())
                .orderId(transferRequest.getOrderId())
                .ownerId(transferRequest.getOwnerId())
                .senderAccountId(transferRequest.getOwnerAccountId())
                .recipientAccountNumber(transferRequest.getClientAccountNumber())
                .amount(transferRequest.getAmount())
                .currencyCode(currencyCode(transferRequest))
                .title(transferRequest.getTitle())
                .build();
    }

    private static String currencyCode(TransferRequest transferRequest) {
        if (transferRequest.getCurrencyCode() == null) {
            return "PLN";
        } else {
            return transferRequest.getCurrencyCode();
        }
    }
}