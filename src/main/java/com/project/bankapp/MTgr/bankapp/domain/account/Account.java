package com.project.bankapp.MTgr.bankapp.domain.account;

import com.project.bankapp.MTgr.bankapp.domain.card.Card;
import com.project.bankapp.MTgr.bankapp.domain.user.User;
import com.project.bankapp.MTgr.bankapp.shared.Auditable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Slf4j
@Getter
@Builder
@Setter(value = AccessLevel.PRIVATE)
public class Account extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "account_sequence")
    @SequenceGenerator(name = "account_sequence")
    private long id;

    @Column(unique = true)
    private String accountNumber;

    private BigDecimal balance;

    private BigDecimal fee;

    @Setter
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Setter
    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @OneToMany(cascade = CascadeType.ALL)
    private final Set<Card> cards = new HashSet<>();

    private final static int INITIAL_AMOUNT = 10000; //FOR TESTING PURPOSE
    private final static int INITIAL_FEE = 10; //FOR TESTING PURPOSE
    private final static int INITIAL_PREMIUM_AMOUNT = 1000000; //FOR TESTING PURPOSE
    private final static int INITIAL_PREMIUM_FEE = 0; //FOR TESTING PURPOSE

    public static Account generateStandardAccount() {
        Account account = new Account();
        account.setAccountNumber("32" + randomAccountNumberCreator() + "00001111");
        account.setBalance(new BigDecimal(INITIAL_AMOUNT));
        account.setFee(new BigDecimal(INITIAL_FEE));
        account.setAccountType(AccountType.STANDARD);
        return account;
    }

    public static Account generatePremiumAccount() {
        Account account = new Account();
        account.setAccountNumber("64" + randomAccountNumberCreator() + "00009999");
        account.setBalance(new BigDecimal(INITIAL_PREMIUM_AMOUNT));
        account.setFee(new BigDecimal(INITIAL_PREMIUM_FEE));
        account.setAccountType(AccountType.PREMIUM);
        return account;
    }

    private static String randomAccountNumberCreator() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return now.format(formatter);
    }

    public boolean isPremium() {
        return AccountType.PREMIUM.equals(accountType);
    }

    public void addFee(BigDecimal amount) {
        this.fee = this.fee.add(amount);
    }

    public void increaseBalance(BigDecimal amountToAdd) {
        this.balance = this.balance.add(amountToAdd);
    }

    public void reduceBalance(BigDecimal amountToSubtract) {
        this.balance = this.balance.subtract(amountToSubtract);
    }

    public Account addCard(Card card) {
        cards.add(card);
        return this;
    }

    public Set<Card> getCards() {
        return cards;
    }

    public Long getId() {
        return id;
    }

    public void accountHasAmountOrThrow(BigDecimal amount) {
        String decline = "*** Declined: Insufficient funds! ***";
        if (!this.isEnoughMoneyToPay(amount)) {
            log.info(decline);
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, decline);
        }
    }

    public boolean isEnoughMoneyToPay(BigDecimal payment) {
        return (balance.subtract(payment).compareTo(BigDecimal.ZERO) >= 0);
    }
}