package com.project.bankapp.MTgr.bankapp.infastructure.transfer;

import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferCreatorCommand;
import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferCreatorCommandMapper;
import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferRetrievalClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.project.bankapp.MTgr.bankapp.api.transfer.TransferRequest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.stream.Collectors;

@Service
class TransferRetrievalRestClient implements TransferRetrievalClient {

    private final RestTemplate restTemplate;
    private final String pendingTransfersUrl;

    @Autowired
    public TransferRetrievalRestClient(RestTemplate restTemplate,
                                       @Value("${pending.transfers.retrieval.auctionapp.url}") String pendingTransfersUrl) {
        this.restTemplate = restTemplate;
        this.pendingTransfersUrl = pendingTransfersUrl;
    }

    @Override
    public List<TransferCreatorCommand> getPendingTransfers() {
        List<TransferRequest> result = restTemplate.exchange(pendingTransfersUrl,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<TransferRequest>>() {
                })
                .getBody();
        return result.stream()
                .map(TransferCreatorCommandMapper::createTransferCommand)
                .collect(Collectors.toList());
    }
}