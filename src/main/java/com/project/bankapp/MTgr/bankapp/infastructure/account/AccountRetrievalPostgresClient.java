package com.project.bankapp.MTgr.bankapp.infastructure.account;

import com.project.bankapp.MTgr.bankapp.domain.account.Account;
import com.project.bankapp.MTgr.bankapp.domain.account.AccountException;
import com.project.bankapp.MTgr.bankapp.domain.account.AccountRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
class AccountRetrievalPostgresClient implements AccountRetrievalClient {

    private final AccountRepository accountRepository;

    @Override
    public Account findById(long accountId) {
        Optional<Account> optionalAccount = accountRepository.findById(accountId);
        return optionalAccount.orElseThrow(() -> AccountException.accountNotFound(accountId));
    }

    @Override
    public Optional<Account> findByAccountNumber(String clientAccountNumber) {
        return accountRepository.findByAccountNumber(clientAccountNumber);
    }
}