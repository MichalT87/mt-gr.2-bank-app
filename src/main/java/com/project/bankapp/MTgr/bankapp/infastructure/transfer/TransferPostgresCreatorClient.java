package com.project.bankapp.MTgr.bankapp.infastructure.transfer;

import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferCreatorClient;
import com.project.bankapp.MTgr.bankapp.domain.transfer.Transfer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class TransferPostgresCreatorClient implements TransferCreatorClient {

    private final TransferRepository transferRepository;

    @Override
    public void create(Transfer transfer) {
        transferRepository.save(transfer);
    }
}