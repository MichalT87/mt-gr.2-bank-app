package com.project.bankapp.MTgr.bankapp.domain.user;

import com.project.bankapp.MTgr.bankapp.api.user.UserRequest;

public class UserCreatorCommandMapper {

    public static UserCreatorCommand createUserCommand(UserRequest userRequest) {
        return UserCreatorCommand.builder()
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .gender(userRequest.getGender())
                .login(userRequest.getLogin())
                .password(userRequest.getPassword())
                .email(userRequest.getEmail())
                .build();
    }

    public static UserResponse mapToDto(User user) {
        return UserResponse.builder()
                .lastName(user.getLastName())
                .firstName(user.getFirstName())
                .login(user.getLogin())
                .gender(user.getGender())
                .password(user.getPassword())
                .email(user.getEmail())
                .build();
    }
}