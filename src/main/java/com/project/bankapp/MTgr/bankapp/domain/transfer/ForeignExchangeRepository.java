package com.project.bankapp.MTgr.bankapp.domain.transfer;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

class ForeignExchangeRepository {

    private static final Map<String, BigDecimal> exchangeRates = new HashMap<>();
    static final String PLN_KEY_NAME = "PLN";
    static final String EUR_KEY_NAME = "EUR";
    static final String USD_KEY_NAME = "USD";
    static final String GBP_KEY_NAME = "GBP";
    static final String CHF_KEY_NAME = "CHF";

    static {
        exchangeRates.put(PLN_KEY_NAME, BigDecimal.valueOf(1));
        exchangeRates.put(EUR_KEY_NAME, BigDecimal.valueOf(4.57));
        exchangeRates.put(USD_KEY_NAME, BigDecimal.valueOf(4.22));
        exchangeRates.put(GBP_KEY_NAME, BigDecimal.valueOf(5.21));
        exchangeRates.put(CHF_KEY_NAME, BigDecimal.valueOf(4.36));
    }

    BigDecimal getEurExchange() {
        return exchangeRates.get(EUR_KEY_NAME);
    }

    BigDecimal getUsdExchange() {
        return exchangeRates.get(USD_KEY_NAME);
    }

    BigDecimal getGbpExchange() {
        return exchangeRates.get(GBP_KEY_NAME);
    }

    BigDecimal getChfExchange() {
        return exchangeRates.get(CHF_KEY_NAME);
    }
}