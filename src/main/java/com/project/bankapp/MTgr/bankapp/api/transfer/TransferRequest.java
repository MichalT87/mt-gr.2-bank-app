package com.project.bankapp.MTgr.bankapp.api.transfer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
@Builder
@JsonDeserialize(builder = TransferRequest.TransferRequestBuilder.class)
public class TransferRequest {

    private final long id;

    private final long orderId;

    @Min(1)
    @NotNull
    private final long ownerId;

    @Min(1)
    @NotNull
    private final long ownerAccountId;

    @NotNull
    private final String clientAccountNumber;

    @NotNull
    @DecimalMin("0.01")
    private final BigDecimal amount;

    private final String currencyCode;

    @NotNull
    @NotEmpty
    private final String title;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TransferRequestBuilder {
    }
}