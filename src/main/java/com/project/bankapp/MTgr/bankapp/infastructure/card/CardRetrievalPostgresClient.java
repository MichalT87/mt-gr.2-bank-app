package com.project.bankapp.MTgr.bankapp.infastructure.card;

import com.project.bankapp.MTgr.bankapp.domain.card.Card;
import com.project.bankapp.MTgr.bankapp.domain.card.CardRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class CardRetrievalPostgresClient implements CardRetrievalClient {

    private final CardRepository cardRepository;

    @Override
    public List<Card> findByAccountId(long accountId) {
        return cardRepository.findAll().stream()
                .filter(card -> card.getAccountId() == accountId)
                .collect(Collectors.toList());
    }

    @Override
    public Card findById(long cardId) {
        return cardRepository.getOne(cardId);
    }
}