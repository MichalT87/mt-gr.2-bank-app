package com.project.bankapp.MTgr.bankapp.infastructure.user;

import com.project.bankapp.MTgr.bankapp.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String login);

    User findByEmail(String email);
}