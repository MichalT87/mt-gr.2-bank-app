package com.project.bankapp.MTgr.bankapp.infastructure.user;

import com.project.bankapp.MTgr.bankapp.domain.user.UserCreatorCommand;
import com.project.bankapp.MTgr.bankapp.domain.user.UserException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.project.bankapp.MTgr.bankapp.domain.user.User;
import com.project.bankapp.MTgr.bankapp.domain.user.UserRetrievalClient;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
class UserRetrievalPostgresClient implements UserRetrievalClient {

    private final UserRepository userRepository;

    @Override
    public User findById(long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        return optionalUser.orElseThrow(
                () -> UserException.userIdNotFound(id));
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByLoginOrCreate(UserCreatorCommand userCreatorCommand) {
        return userRepository.findByLogin(userCreatorCommand.getLogin())
                .orElse(User.createUser(userCreatorCommand));
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}