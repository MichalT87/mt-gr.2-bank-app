package com.project.bankapp.MTgr.bankapp.domain.card;

import com.project.bankapp.MTgr.bankapp.domain.account.Account;
import com.project.bankapp.MTgr.bankapp.domain.account.AccountRetrievalClient;
import com.project.bankapp.MTgr.bankapp.domain.user.User;
import com.project.bankapp.MTgr.bankapp.domain.user.UserException;
import com.project.bankapp.MTgr.bankapp.domain.user.UserRetrievalClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardStatusChanger implements StatusChanger {

    private final UserRetrievalClient userRetrievalClient;
    private final CardRetrievalClient cardRetrievalClient;
    private final AccountRetrievalClient accountRetrievalClient;

    @Override
    public void restrict(long accountId, String lastFourDigits) {
        restrictByFourLastDigits(accountId, lastFourDigits);
    }

    @Transactional
    public void restrictByFourLastDigits(long accountId, String lastFourDigits) {
        List<Card> cards = cardRetrievalClient.findByAccountId(accountId);
        if (cards.stream()
                .filter(card -> card.getLastFourDigits().equals(lastFourDigits))
                .anyMatch(Card::isActive)) {
            cards.stream()
                    .filter(card -> card.getLastFourDigits().equals(lastFourDigits))
                    .filter(Card::isActive)
                    .findFirst()
                    .ifPresent(Card::restrict);
            log.info("Card with last four digits: {} from account with id: {} has been restricted.", lastFourDigits, accountId);
        } else {
            throw CardException.cardAlreadyRestricted(accountId, lastFourDigits);
        }
    }

    @Override
    public void restrictAllCardsByUser(long userId) {
        restrictAllActiveCardsByUser(userId);
        log.info("All user's {} cards has been restricted.", userId);
    }

    @Transactional
    public void restrictAllActiveCardsByUser(long userId) {
        User user = userRetrievalClient.findById(userId);
        user.getAccounts().stream()
                .map(Account::getCards)
                .flatMap(Collection::stream)
                .filter(Card::isActive)
                .forEach(Card::restrict);
    }

    @Override
    public void restrictAllCardsByAccount(long accountId) {
        restrictAllActiveCardsByAccount(accountId);
        log.info("All cards from account {} has been restricted.", accountId);
    }

    @Transactional
    public void restrictAllActiveCardsByAccount(long accountId) {
        Account account = accountRetrievalClient.findById(accountId);
        account.getCards().stream()
                .filter(Card::isActive)
                .forEach(Card::restrict);
    }
}