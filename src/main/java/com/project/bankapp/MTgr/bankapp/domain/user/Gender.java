package com.project.bankapp.MTgr.bankapp.domain.user;

public enum Gender {
    MALE, FEMALE
}