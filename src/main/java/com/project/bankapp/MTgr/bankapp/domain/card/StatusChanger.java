package com.project.bankapp.MTgr.bankapp.domain.card;

public interface StatusChanger {

    void restrict(long accountId, String lastFourDigits);

    void restrictAllCardsByUser(long userId);

    void restrictAllCardsByAccount(long accountId);
}