package com.project.bankapp.MTgr.bankapp.domain.user;

import java.util.List;
import java.util.Optional;

public interface UserRetrievalClient {

    User findById(long id);

    User findByEmail(String email);

    Optional<User> findByLogin(String login);

    List<User> getAllUsers();

    User findByLoginOrCreate(UserCreatorCommand userCreatorCommand);
}