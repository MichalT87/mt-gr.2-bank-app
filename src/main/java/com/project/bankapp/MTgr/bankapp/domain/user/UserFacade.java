package com.project.bankapp.MTgr.bankapp.domain.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private final UserCreator userCreator;
    private final UserRetrievalClient userRetrievalClient;

    public void createUser(UserCreatorCommand userCreatorCommand) {
        userCreator.createUser(userCreatorCommand);
    }

    public UserResponse getUserById(long userId) {
        User user = userRetrievalClient.findById(userId);
        return user.generateResponse();
    }

    public UserResponse getUserByEmail(String email) {
        User user = userRetrievalClient.findByEmail(email);
        return user.generateResponse();
    }

    public List<UserResponse> getAllUsers() {
        return userRetrievalClient.getAllUsers()
                .stream()
                .map(User::generateResponse)
                .collect(Collectors.toList());
    }
}