package com.project.bankapp.MTgr.bankapp.infastructure.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.project.bankapp.MTgr.bankapp.domain.user.UserCreatorClient;
import com.project.bankapp.MTgr.bankapp.domain.user.User;

@Service
@RequiredArgsConstructor
class UserPostgresCreatorClient implements UserCreatorClient {

    private final UserRepository userRepository;

    @Override
    public void create(User user) {
        userRepository.save(user);
    }
}