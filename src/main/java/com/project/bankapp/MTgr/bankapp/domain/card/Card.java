package com.project.bankapp.MTgr.bankapp.domain.card;

import com.project.bankapp.MTgr.bankapp.shared.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "CARDS")
public class Card extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "card_sequence")
    @SequenceGenerator(name = "card_sequence")
    private long id;
    private long accountId;
    private String cardNumber;
    @Column(unique = true)
    private String lastFourDigits;
    private String cvv;
    private LocalDate expirationDate;
    @Enumerated(EnumType.STRING)
    public Status status;

    public static Card generateCard(long accountId) {
        String lastFourDigits = randomNDigits(4);
        return Card.builder()
                .accountId(accountId)
                .lastFourDigits(lastFourDigits)
                .cardNumber("1234-0000-0000-" + lastFourDigits)
                .cvv(randomNDigits(3))
                .expirationDate(LocalDate.now().plusYears(5))
                .status(Status.ACTIVE)
                .build();
}

    public boolean isActive() {
        return status.equals(Status.ACTIVE);
    }

    public void activate() {
        this.status = Status.ACTIVE;
    }

    public void restrict() {
        this.status = Status.RESTRICTED;
    }

    public static String randomNDigits(int n) {
        long q = Math.round(Math.random() * (int)Math.pow(10, n));
        String digitsIn4digits = String.valueOf((int) q);
        int neededZeros = n - String.valueOf((int) q).length();
        String last4digits = "";
        for (int i = 0; i < neededZeros; i++) {
            last4digits = last4digits.concat("0");
        }
        last4digits = last4digits.concat(digitsIn4digits);
        return last4digits;
    }
}