package com.project.bankapp.MTgr.bankapp.domain.card;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CardFacade {

    private final StatusChanger statusChanger;

    @Transactional
    public void restrict(long accountId, String lastFourDigits) {
        statusChanger.restrict(accountId, lastFourDigits);
    }
}