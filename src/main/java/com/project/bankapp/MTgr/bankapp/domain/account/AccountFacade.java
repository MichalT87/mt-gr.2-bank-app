package com.project.bankapp.MTgr.bankapp.domain.account;

import com.project.bankapp.MTgr.bankapp.domain.card.CardCreator;
import com.project.bankapp.MTgr.bankapp.domain.card.StatusChanger;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AccountFacade {

    private final AccountCreator accountCreator;
    private final CardCreator cardCreator;
    private final StatusChanger statusChanger;

    public void createStandardAccount(long userId){
        addCard(accountCreator.createStandardAccountAndAssignToUser(userId).getId());
    }

    public void createPremiumAccount(long userId){
        addCard(accountCreator.createPremiumAccountAndAssignToUser(userId).getId());
    }

    public void addCard(long accountId){
        cardCreator.addCard(accountId);
    }

    @Transactional
    public void restrictAllCardsByUser(long userId) {
        statusChanger.restrictAllCardsByUser(userId);
    }

    @Transactional
    public void restrictAllCardsByAccount(long accountId) {
        statusChanger.restrictAllCardsByAccount(accountId);
    }
}