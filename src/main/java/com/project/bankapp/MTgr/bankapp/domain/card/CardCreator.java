package com.project.bankapp.MTgr.bankapp.domain.card;

import com.project.bankapp.MTgr.bankapp.domain.account.Account;
import com.project.bankapp.MTgr.bankapp.domain.account.AccountRetrievalClient;
import com.project.bankapp.MTgr.bankapp.domain.account.FeesCharger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardCreator {

    private final AccountRetrievalClient accountRetrievalClient;
    private final FeesCharger feesCharger;

    @Transactional
    public void addCard(long accountId) {
        Account account = accountRetrievalClient.findById(accountId);
        account.addCard(Card.generateCard(accountId));
        feesCharger.addNewCardFee(account);
        log.info("Create credit card for account with id: {}.", accountId);
    }
}