package com.project.bankapp.MTgr.bankapp.api.user;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Value
public class UserRequest {

    @NotNull
    @NotEmpty
    private final String firstName;

    @NotNull
    @NotEmpty
    private final String lastName;

    @NotNull
    private final String gender;

    @NotNull
    @NotEmpty
    private final String login;

    @NotBlank
    private final String password;

    @Email
    private final String email;
}