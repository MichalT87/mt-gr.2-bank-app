package com.project.bankapp.MTgr.bankapp.domain.account;

import java.util.Optional;

public interface AccountRetrievalClient {

    Account findById(long accountId);

    Optional<Account> findByAccountNumber(String accountNumber);
}