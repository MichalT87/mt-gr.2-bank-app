package com.project.bankapp.MTgr.bankapp.domain.card;

import java.util.List;

public interface CardRetrievalClient {

    Card findById(long cardId);

    List<Card> findByAccountId(long accountId);
}