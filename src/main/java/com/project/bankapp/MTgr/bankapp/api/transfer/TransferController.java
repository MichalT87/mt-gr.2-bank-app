package com.project.bankapp.MTgr.bankapp.api.transfer;

import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferCreatorCommand;
import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferCreatorCommandMapper;
import com.project.bankapp.MTgr.bankapp.domain.transfer.TransferFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping("/transfers")
@RequiredArgsConstructor
class TransferController {

    private final TransferFacade transferFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransfer(@Valid @RequestBody TransferRequest transferRequest) {
        TransferCreatorCommand transferCreatorCommand = TransferCreatorCommandMapper.createTransferCommand(transferRequest);
        transferFacade.createTransfer(transferCreatorCommand);
    }

    @PostMapping(path = "/order")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOrderTransfer(@Valid @RequestBody TransferRequest transferRequest) {
        TransferCreatorCommand transferCreatorCommand = TransferCreatorCommandMapper.createTransferCommand(transferRequest);
        transferFacade.createOrderTransfer(transferCreatorCommand);
    }

    @PostMapping(path = "/pending")
    @ResponseStatus(HttpStatus.CREATED)
    public void pullPendingTransferList() {
        transferFacade.createTransfersToProcess();
    }
}