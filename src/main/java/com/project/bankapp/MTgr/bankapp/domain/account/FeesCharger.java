package com.project.bankapp.MTgr.bankapp.domain.account;

import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Service
public class FeesCharger {

    private static AccountRetrievalClient accountRetrievalClient;

    public void addNewCardFee(Account account) {
        if (account.isPremium() && account.getCards().size() >= 10 || !account.isPremium()) {
            account.addFee(BigDecimal.TEN);
        }
    }

    public static void addExternalTransferFee(long accountId) {
        Account account = accountRetrievalClient.findById(accountId);
        account.addFee(BigDecimal.TEN);
    }
}