package com.project.bankapp.MTgr.bankapp.domain.transfer;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
public class TransferCreatorCommand {

    private final long id;

    private final long orderId;

    @NonNull
    private long ownerId;

    @NonNull
    private long senderAccountId;

    @NonNull
    private String recipientAccountNumber;

    @NonNull
    private BigDecimal amount;

    private String currencyCode;

    @NonNull
    private String title;
}