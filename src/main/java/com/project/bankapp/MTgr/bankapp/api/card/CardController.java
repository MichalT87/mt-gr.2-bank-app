package com.project.bankapp.MTgr.bankapp.api.card;

import com.project.bankapp.MTgr.bankapp.domain.account.AccountFacade;
import com.project.bankapp.MTgr.bankapp.domain.card.CardFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/cards")
@RequiredArgsConstructor
class CardController {

    private final AccountFacade accountFacade;
    private final CardFacade cardFacade;

    @PostMapping(path = "/{accountId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addCardToAccount(@Min(1) @PathVariable long accountId) {
        accountFacade.addCard(accountId);
    }

    @PostMapping(path = "/restrictCard/{accountId}/{lastFourDigits}")
    @ResponseStatus(HttpStatus.OK)
    public void restrictCard(@Min(1) @PathVariable long accountId, @PathVariable String lastFourDigits) {
        cardFacade.restrict(accountId, lastFourDigits);
    }

    @PostMapping(path = "/restrictAllUserCards/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void restrictAllUserCards(@Min(1) @PathVariable long userId) {
        accountFacade.restrictAllCardsByUser(userId);
    }

    @PostMapping(path = "/restrictAllCards/{accountId}")
    @ResponseStatus(HttpStatus.OK)
    public void restrictAllAccountsCards(@Min(1) @PathVariable long accountId) {
        accountFacade.restrictAllCardsByAccount(accountId);
    }
}