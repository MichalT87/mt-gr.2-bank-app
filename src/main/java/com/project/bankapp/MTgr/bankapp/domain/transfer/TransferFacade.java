package com.project.bankapp.MTgr.bankapp.domain.transfer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TransferFacade {

    private final TransferCreator transferCreator;

    public void createTransfer(TransferCreatorCommand transferCreatorCommand) {
        transferCreator.sendToCreateTransferAndExecute(transferCreatorCommand);
    }

    public void createOrderTransfer(TransferCreatorCommand transferCreatorCommand) {
        transferCreator.createOrderTransfer(transferCreatorCommand);
    }

    public void createTransfersToProcess() {
        transferCreator.createPendingTransfers();
    }
}