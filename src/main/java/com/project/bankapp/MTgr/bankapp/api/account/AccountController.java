package com.project.bankapp.MTgr.bankapp.api.account;

import com.project.bankapp.MTgr.bankapp.domain.account.AccountFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
class AccountController {

    private final AccountFacade accountFacade;

    @PostMapping(path = "/users/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void createAccount(@Min(1) @PathVariable long userId) {
        accountFacade.createStandardAccount(userId);
    }

    @PostMapping(path = "/premium/users/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void createPremiumAccount(@Min(1) @PathVariable long userId) {
        accountFacade.createPremiumAccount(userId);
    }
}