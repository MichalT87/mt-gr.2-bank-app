package com.project.bankapp.MTgr.bankapp.infastructure.transfer;

import com.project.bankapp.MTgr.bankapp.domain.transfer.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
}