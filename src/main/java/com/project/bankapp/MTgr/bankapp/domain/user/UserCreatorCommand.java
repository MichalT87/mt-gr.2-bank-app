package com.project.bankapp.MTgr.bankapp.domain.user;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Getter
public class UserCreatorCommand {
    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final String gender;

    @NotNull
    private final String login;

    @NotBlank
    private final String password;

    @Email
    private final String email;
}