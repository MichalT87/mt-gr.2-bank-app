package com.project.bankapp.MTgr.bankapp.infastructure.account;

import com.project.bankapp.MTgr.bankapp.domain.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByAccountNumber(String accountNumber);
}