package com.project.bankapp.MTgr.bankapp.domain.user;

public class UserException extends RuntimeException {

    public UserException(String message) {
        super(message);
    }

    public static UserException userIdNotFound(long id) {
        return new UserException(String.format
                ("User not found [id=%s]", id));
    }

    public static UserException loginAlreadyExistsException(String login) {
        return new UserException(String.format
                ("This login is taken. Cannot create user %s", login));
    }
}