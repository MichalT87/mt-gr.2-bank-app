package com.project.bankapp.MTgr.bankapp.domain.transfer;

import com.project.bankapp.MTgr.bankapp.shared.Auditable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Table(name = "TRANSFER")
public class Transfer extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "transfer_sequence")
    @SequenceGenerator(name = "transfer_account")
    @Setter(value = AccessLevel.PRIVATE)
    private long id;
    private long orderId;
    private long ownerId;
    @Setter(AccessLevel.PRIVATE)
    private long senderAccountId;
    @Setter(AccessLevel.PRIVATE)
    private String recipientAccountNumber;
    private BigDecimal amount;
    private String currencyCode;
    private String title;
    private BigDecimal exchangeRate;

    private static final ForeignExchangeRepository FOREIGN_EXCHANGE_REPOSITORY = new ForeignExchangeRepository();

    public static Transfer createTransferOfMoney(TransferCreatorCommand transferCreatorCommand) {
        Transfer transfer = createTransfer(transferCreatorCommand);
        switch (transferCreatorCommand.getCurrencyCode()) {
            case ForeignExchangeRepository.EUR_KEY_NAME:
                return createEurTransfer(transfer);
            case ForeignExchangeRepository.USD_KEY_NAME:
                return createUsdTransfer(transfer);
            case ForeignExchangeRepository.GBP_KEY_NAME:
                return createGbpTransfer(transfer);
            case ForeignExchangeRepository.CHF_KEY_NAME:
                return createChfTransfer(transfer);
            default:
                return transfer;
        }
    }

    private static Transfer createEurTransfer(Transfer transfer) {
        transfer.setExchangeRate(FOREIGN_EXCHANGE_REPOSITORY.getEurExchange());
        transfer.setAmount(transfer.getAmount().multiply(transfer.exchangeRate));
        return transfer;
    }

    private static Transfer createUsdTransfer(Transfer transfer) {
        transfer.setExchangeRate(FOREIGN_EXCHANGE_REPOSITORY.getUsdExchange());
        transfer.setAmount(transfer.getAmount().multiply(transfer.exchangeRate));
        return transfer;
    }

    private static Transfer createGbpTransfer(Transfer transfer) {
        transfer.setExchangeRate(FOREIGN_EXCHANGE_REPOSITORY.getGbpExchange());
        transfer.setAmount(transfer.getAmount().multiply(transfer.exchangeRate));
        return transfer;
    }

    private static Transfer createChfTransfer(Transfer transfer) {
        transfer.setExchangeRate(FOREIGN_EXCHANGE_REPOSITORY.getChfExchange());
        transfer.setAmount(transfer.getAmount().multiply(transfer.exchangeRate));
        return transfer;
    }

    public static Transfer createTransfer(TransferCreatorCommand transferCreatorCommand) {
        return Transfer.builder()
                .orderId(transferCreatorCommand.getOrderId())
                .ownerId(transferCreatorCommand.getOwnerId())
                .senderAccountId(transferCreatorCommand.getSenderAccountId())
                .recipientAccountNumber(transferCreatorCommand.getRecipientAccountNumber())
                .amount(transferCreatorCommand.getAmount())
                .currencyCode(transferCreatorCommand.getCurrencyCode())
                .title(transferCreatorCommand.getTitle())
                .build();
    }
}