package com.project.bankapp.MTgr.bankapp.domain.transfer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
class TransferCreator {

    private final TransferRetrievalClient transferRetrievalClient;
    private final TransferExecutor transferExecutor;
    private final TransferStatusResponseClient transferStatusResponseClient;

    public void sendToCreateTransferAndExecute(TransferCreatorCommand transferCreatorCommand) {
        transferExecutor.createAndExecuteTransfer(transferCreatorCommand);
    }

    public void sendOrderStatusChange(TransferCreatorCommand transferCreatorCommand) {
        transferStatusResponseClient.sendStatusChange(transferCreatorCommand.getOrderId());
    }

    @Transactional
    public void createOrderTransfer(TransferCreatorCommand transferCreatorCommand) {
        sendToCreateTransferAndExecute(transferCreatorCommand);
        sendOrderStatusChange(transferCreatorCommand);
    }

    void createPendingTransfers() {
        List<TransferCreatorCommand> transfersToProcess = transferRetrievalClient.getPendingTransfers();
        for (TransferCreatorCommand transferCommand : transfersToProcess) {
            createOrderTransfer(transferCommand);
        }
        log.info("Pulling pending transfers from the auction app.");
    }
}