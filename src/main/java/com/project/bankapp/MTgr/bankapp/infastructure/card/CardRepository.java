package com.project.bankapp.MTgr.bankapp.infastructure.card;

import com.project.bankapp.MTgr.bankapp.domain.card.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Long> {

    List<Card> findByAccountId(long accountId);

    Card findById(long cardId);
}