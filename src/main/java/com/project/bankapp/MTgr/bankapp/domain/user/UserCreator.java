package com.project.bankapp.MTgr.bankapp.domain.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
class UserCreator {

    private final UserCreatorClient userCreatorClient;
    private final UserRetrievalClient userRetrievalClient;

    @Transactional
    public void createUser(UserCreatorCommand userCreatorCommand) {
        ifLoginExistsThrow(userCreatorCommand.getLogin());
        User user = userRetrievalClient.findByLoginOrCreate(userCreatorCommand);
        userCreatorClient.create(user);
        log.info("New User: *{} {}* has been created.", userCreatorCommand.getFirstName(), userCreatorCommand.getLastName());
    }

    private void ifLoginExistsThrow(String login) {
        if (userRetrievalClient.findByLogin(login).isPresent()) {
            throw UserException.loginAlreadyExistsException(login);
        }
    }
}